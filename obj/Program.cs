﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiabloStripped
{
    class Program
    {
        static void Main(String[] args)
        {
   
            ChooseYourHero();
        }

        /// <summary>
        /// The PrintHeroStats method takes the chosen hero as its parameter and prints their stats
        /// It uses a stringbuilder to sort of concatenate the different attributes
        /// It prints every time a hero levels up
        ///</summary>
        public static void PrintHeroStats(Hero hero)
        {
            Console.WriteLine();
            StringBuilder HeroStats = new($"You are a {hero.ClassName} in level {hero.Level}\n");

            HeroStats.Append($"Primary stats: \n");
            HeroStats.Append($"-----Viality: {hero.Vitality}\n");
            HeroStats.Append($"-----Strength: {hero.Strength}\n");
            HeroStats.Append($"-----Dexterity: {hero.Dexterity}\n");
            HeroStats.Append($"-----Intelligence: {hero.Intelligence}\n");

            HeroStats.Append($"-----Health: {hero.Health}\n");

            Console.WriteLine(HeroStats);

        }

        /// <summary>
        ///The Main method calls on the ChooseYourHero method, which creates an instance of a hero
        ///Based on user input the new class is created
        ///It then calls the Run method using the chosen hero as a parameter
        /// </summary>

        public static void ChooseYourHero()
        {
            Console.WriteLine("Choose your hero class: Mage, Ranger, Rogue, Warrior");
            string input = Console.ReadLine();

            Hero hero;
            switch (input.ToLower())
            {
                case "mage":
                    hero = new Ranger();
                    Run(hero);
                    break;

                case "ranger":
                    hero = new Ranger();
                    Run(hero);
                    break;

                case "rogue":
                    hero = new Ranger();
                    Run(hero);
                    break;

                case "warrior":
                    hero = new Ranger();
                    Run(hero);
                    break;

                default:
                    Console.WriteLine("Please choose a valid class");
                    break;

            }
        }

        /// <summary>
        /// Run takes on the chosen hero as a parameter and calls on the PrintHeroStats using this param again
        /// This method will not stop running, which means that you can level up your hero multiple times by writing "lvl"
        /// It also allows the player to equip a weapon or armor
        /// </summary>

        public static void Run(Hero hero)
        {
            PrintHeroStats(hero);
            while (true)
            {
                string input = Console.ReadLine().ToLower();
                switch (input)
                {
                    case "lvl":
                        hero.LevelUp();
                        PrintHeroStats(hero);
                        break;
                    case "equip":
                        hero.Equip();
                        break;
                }
            }
        }

        
    }
}

