﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiabloStripped
{
    public class Weapons : Items
    {
        //Attributes for the weapons
        public double AttackSpeed { get; set; }
        public int WeaponDamage { get; set; }


        public EquipableWeapons WeaponType { get; set; }

        /// <summary>
        /// The constructor takes a type from the Enum "EquipableWeapon" and assigns it
        /// It also calls on the WeaponAtts method which uses this type to assign values
        /// </summary>
        public Weapons(EquipableWeapons type)
        {
            WeaponType = type;
            WeaponAtt();
        }

        public List<Weapons.EquipableWeapons> Aweapon { get; set; }

        /// <summary>
        /// The WeaponAtt method gives every weapon inside the Enum "EquipableWeapons" a name and the appropriate attribute values
        /// It uses the type given in the constructor
        /// </summary>
        public void WeaponAtt()
        {
            switch (WeaponType)
            {
                case EquipableWeapons.Axe:
                    this.ItemName = "Axe";
                    this.AttackSpeed = 1.1;
                    this.ItemLevel = 1;
                    this.WeaponDamage = 7;
                    break;

                case EquipableWeapons.Bow:
                    this.ItemName = "Bow";
                    this.AttackSpeed = 0.8;
                    this.ItemLevel = 1;
                    this.WeaponDamage = 12;
                    break;

                case EquipableWeapons.Dagger:
                    this.ItemName = "Dagger";
                    this.AttackSpeed = 2;
                    this.ItemLevel = 1;
                    this.WeaponDamage = 4;
                    break;

                case EquipableWeapons.Hammer:
                    this.ItemName = "Hammer";
                    this.AttackSpeed = 0.8;
                    this.ItemLevel = 1;
                    this.WeaponDamage = 7;
                    break;

                case EquipableWeapons.Staff:
                    this.ItemName = "Staff";
                    this.AttackSpeed = 1.8;
                    this.ItemLevel = 1;
                    this.WeaponDamage = 8;
                    break;

                case EquipableWeapons.Sword:
                    this.ItemName = "Sword";
                    this.AttackSpeed = 1.5;
                    this.ItemLevel = 1;
                    this.WeaponDamage = 10;
                    break;

                case EquipableWeapons.Wand:
                    this.ItemName = "Wand";
                    this.AttackSpeed = 1.6;
                    this.ItemLevel = 1;
                    this.WeaponDamage = 4;
                    break;

                default:
                    Console.WriteLine($"Please choose a valid weapon");
                    break;
            }
        }
    }
}
