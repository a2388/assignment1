﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiabloStripped
{
    public abstract class Items
    {
        //ItemName attribute is inherited by both the Armor and Weapons class
        public string ItemName { get; set; }
        public int ItemLevel { get; set; }

        // Enums with the appropriate objects(??)
        public enum Slots
        {
            Head,
            Body,
            Legs,
            Weapon
        }

        public enum EquipableWeapons
        {
            Axe,
            Bow,
            Dagger,
            Hammer,
            Staff,
            Sword,
            Wand
        }

        public enum EquipableArmor
        {
            Cloth,
            Leather,
            Mail,
            Plate
        }

    }
}
