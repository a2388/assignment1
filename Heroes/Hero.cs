﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiabloStripped
{
    public abstract class Hero
    {
        public string ClassName { get; set; }

        public int Level = 1;

        //Primary stats
        public int Vitality { get; set; }
        public int Strength { get; set; }
        public int Dexterity { get; set; }
        public int Intelligence { get; set; }

        public int TotalVitality { get; set; }
        public int TotalStrength { get; set; }
        public int TotalDexterity { get; set; }
        public int TotalIntelligence { get; set; }

        //Seconday stats
        public int Health { get; set; }
        public int ArmorRating { get; set; }
        public int ElementResistance { get; set; }


        public string DesiredSlot { get; set; }


        public Dictionary<Object, Object> EquipmentSlots = new Dictionary<Object, Object>();

        public abstract void LevelUp();

        public abstract void EquipWeapon(Weapons weapon);

        public abstract void EquipArmor(Armor armor);



        public void PrimaryAtt(int Vitality, int Strength, int Dexterity, int Intelligence)
        {
            this.Vitality = Vitality;
            this.Strength = Strength;
            this.Dexterity = Dexterity;
            this.Intelligence = Intelligence;

            this.TotalVitality += Vitality;
            this.TotalStrength += Strength;
            this.TotalDexterity += Dexterity;
            this.TotalIntelligence += Intelligence;
        }

        public void SecondaryAtt()
        {
            this.Health = TotalVitality * 10;
            this.ArmorRating = TotalStrength + TotalDexterity;
            this.ElementResistance = TotalIntelligence;
        }
        public void Equip()
        {
            //Prompts user input and sets it to a variable
            Console.Write("What would you like to equip? ");
            Console.WriteLine();
            string input = Console.ReadLine();

            //Checks if user input exists in Armor Enum and then if the desired slot exists
            //in Slots enum
            if (input == "armor")
            {
                Console.WriteLine("Please choose armor [Leather, Cloth, Mail, Plate]");
                string armor = Console.ReadLine().ToLower();
                Armor.EquipableArmor chosenArmor = (Armor.EquipableArmor)Enum.Parse(typeof(Armor.EquipableArmor), armor, true);
                if (Enum.IsDefined(typeof(Items.EquipableArmor), chosenArmor))
                {
                    Armor newArmor = new(chosenArmor);
                    EquipArmor(newArmor);
                }
                else
                {
                    throw new InvalidArmorException("Invalid armor choice");
                }
            }
            else if (input == "weapon")
            {
                Console.WriteLine("Please choose a weapon [Axe, Bow, Dagger, Hammer, Staff, Wand, sword]");
                string weapon = Console.ReadLine().ToLower();
                Weapons.EquipableWeapons chusenVepon = (Weapons.EquipableWeapons)Enum.Parse(typeof(Weapons.EquipableWeapons), weapon, true);
                if (Enum.IsDefined(typeof(Items.EquipableWeapons), chusenVepon))
                {
                    Weapons newWeapon = new(chusenVepon);
                    EquipWeapon(newWeapon);
                }
                else
                {
                    throw new InvalidArmorException("Invalid armor choice");

                }
            }else
            {
                Console.WriteLine();
                Console.WriteLine("Please choose between armor or weapon");
                Equip();
            }
        }
      
    }
}
