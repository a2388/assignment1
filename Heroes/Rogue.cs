﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiabloStripped
{
    public class Rogue : Hero
    {
        //The constructor sets the classname of the class and calls the PrimaryAtt method, which sets the primary attributes
        public Rogue()
        {
            ;
            this.ClassName = "Rogue";
            PrimaryAtt(8, 2, 6, 1);
        }

        //Increases the attributes for the class
        public override void LevelUp()
        {
            this.Level += 1;
            this.Vitality += 3;
            this.Strength += 1;
            this.Dexterity += 4;
            this.Intelligence += 1;
        }

        /// <summary>
        /// The EquipArmor method takes a parameter from the armor class
        /// it starts of by manually checking if the chosen armor is allowed for the Ranger class and if the hero has the appropriate
        /// level to equip this armor
        /// If this is the case the method asked where the player wants to equip the armor
        /// It turns the input from the player into an object and checks if the input exists in the Enum "slots" and then alots it to the dictionary "EquipmentSlots"
        /// If the input is invalid an exception is thrown
        /// </summary>
        public override void EquipArmor(Armor armor)
        {
            if (armor.ArmorType == Items.EquipableArmor.Leather || armor.ArmorType == Items.EquipableArmor.Mail && armor.ItemLevel <= Level)
            {
                Console.WriteLine($"In which slot would you like to equip {armor.ArmorType}");
                DesiredSlot = Console.ReadLine().ToLower(); ;

                //Armor can currently be equiped in the weapon slot *upsi
                Items.Slots slots = (Items.Slots)Enum.Parse(typeof(Items.Slots), DesiredSlot, true);
                if (Enum.IsDefined(typeof(Items.Slots), slots))
                {
                    EquipmentSlots.Add(DesiredSlot, armor);
                    TotalVitality += armor.UpVitality;
                    TotalStrength += armor.UpStrength;
                    TotalDexterity += armor.UpDexterity;
                    TotalIntelligence += armor.UpIntelligence;

                    Console.WriteLine(TotalDexterity);

                    Console.WriteLine($"{armor.ArmorType} has been equipped in the {slots} slot");
                    //TotalVitality += Convert.ToInt32(EquipmentSlots.GetValueOrDefault(armor.UpVitality));
                    //Console.WriteLine(EquipmentSlots.TryGetValue(DesiredSlot, out DesiredArmor));
                }
                else
                {
                    throw new InvalidArmorException("Invalid slot");
                }
            }
            else
            {
                //If the input is invalid an exception is thrown
                throw new InvalidArmorException("Restricted armor for this class");
            }
        }

        /// <summary>
        /// The EquipWeapon method takes a parameter from the Weapon class
        /// It starts of by manually checking if the chosen weapon is allowed for the Ranger class and if the hero has the appropriate
        /// level to equip it
        /// If this is the case the weapon is added to the weapon slot in the dictionary "EquipmentSlots"
        /// If the input is invalid an exception is thrown
        /// </summary>
        public override void EquipWeapon(Weapons weapon)
        {
            if (weapon.WeaponType == Items.EquipableWeapons.Dagger || weapon.WeaponType == Items.EquipableWeapons.Sword && weapon.ItemLevel <= Level)
            {

                EquipmentSlots.Add(Items.Slots.Weapon, weapon);
                Console.WriteLine($"{weapon.ItemName} has been equipped");
                double DPS = weapon.WeaponDamage * weapon.AttackSpeed;
            }
            else
            {
                //If the input is invalid an exception is thrown
                throw new InvalidWeaponException("Restricted weapon for this class");
            }
        }
    }
}

