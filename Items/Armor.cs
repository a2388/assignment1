﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DiabloStripped
{
    public class Armor : Items
    {
        //The attributes for the armor that will increse the players primary stats
        public int UpVitality { get; set; }
        public int UpIntelligence { get; set; }
        public int UpDexterity { get; set; }
        public int UpStrength { get; set; }

        public int armorStrength { get; set; }


        public EquipableArmor ArmorType { get; set; }


        /// <summary>
        /// The constructor takes a type from the Enum "EquipableArmor" and assigns it
        /// It also calls on the ArmorAtts method which uses this type to assign values
        /// </summary>
        public Armor(EquipableArmor type)
        {
            ArmorType = type;
            ArmorAtts();
        }

        /// <summary>
        /// This method gives every armor inside the Enum "EquipableArmor" a name and the appropriate attribute values
        /// It uses the ArmorType given in the constructor
        /// </summary>
        public void ArmorAtts()
        {
            switch (ArmorType)
            {
                case EquipableArmor.Cloth:
                    this.ItemName = "Common Cloth";
                    this.UpVitality = 1;
                    this.UpIntelligence = 5;
                    break;

                case EquipableArmor.Leather:
                    this.ItemName = "Common Leather";
                    this.UpDexterity = 4;
                    this.UpStrength = 5;
                    break;

                case EquipableArmor.Mail:
                    this.ItemName = "Common Mail";
                    this.UpDexterity = 3;
                    this.UpStrength = 2;
                    break;

                case EquipableArmor.Plate:
                    this.ItemName = "Common Plate";
                    this.UpVitality = 1;
                    this.UpStrength= 2;
                    break;

                default:
                    Console.WriteLine("Please choose appropriate armor!");
                    break;
            }
        }
    }
}
